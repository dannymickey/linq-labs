from py_linq import Enumerable
import time
start = time.time()
from collections127 import product, pc, laptop, printer


print(
Enumerable([{'average':
Enumerable([
{'price': laptop.join( \
        product, lambda s1: s1['model'], lambda s2: s2['model'], lambda result: result \
    ).where( \
        lambda x: x[1]['maker'] in \
                    pc.join(product, lambda s1: s1['model'], lambda s2: s2['model'], lambda result: result) \
                    .where(lambda x: x[0]['cd'] == pc.min(lambda y: y['cd'])) \
                    .select(lambda x: x[1]['maker']) \
                    .to_list() \
    ).order_by(lambda x: x[0]['price']) \
    .select(lambda x: x[0]['price']) \
    .take(1) \
    .to_list()[0]
},
{'price': pc.join( \
        product, lambda s1: s1['model'], lambda s2: s2['model'], lambda result: result \
    ).where( \
        lambda x: x[1]['maker'] in \
                    printer.join(product, lambda s1: s1['model'], lambda s2: s2['model'], lambda result: result) \
                    .where(lambda x: x[0]['price'] == printer.min(lambda y: y['price'])) \
                    .select(lambda x: x[1]['maker']) \
                    .to_list() \
    ).order_by_descending(lambda x: x[0]['price']) \
    .select(lambda x: x[0]['price']) \
    .take(1)
    .to_list()[0]
},
{'price': printer.join( \
        product, lambda s1: s1['model'], lambda s2: s2['model'], lambda result: result \
    ).where( \
        lambda x: x[1]['maker'] in \
                    laptop.join(product, lambda s1: s1['model'], lambda s2: s2['model'], lambda result: result) \
                    .where(lambda x: x[0]['ram'] == laptop.max(lambda y: y['ram'])) \
                    .select(lambda x: x[1]['maker']) \
                    .to_list() \
    ).order_by_descending(lambda x: x[0]['price']) \
    .select(lambda x: x[0]['price']) \
    .take(1)
    .to_list()[0]
}
]).avg(lambda x: x['price']) }]).select(lambda x: round(x['average'], 2)).to_list()[0]
)
end = time.time()
print("Execution time: " + str(end - start))
