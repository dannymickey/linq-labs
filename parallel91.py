import threading
from py_linq import Enumerable
from queue import Queue
import time
start = time.time()

from collections91 import utQ, utV, utB

def getAverage(k, results):
    if k == 0:
        results[k] = utB.sum(lambda x: x['B_VOL'])
    elif k == 1:
        results[k] = utQ.count()
    elif k == 2:
        results[k] = Enumerable([ \
            { 'division': (results[0] / results[1]) } \
        ]).select(lambda x: round(x['division'], 2)).to_list()[0]


results = [0,0,0]
for i in range(3):
    t = threading.Thread(target=getAverage, args = (i,results))
    t.start()

print(results[2])
end = time.time()
print("Execution time: " + str(end - start))
