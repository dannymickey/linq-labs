import threading
from py_linq import Enumerable
from queue import Queue
import time
start = time.time()
from collections127 import product, pc, laptop, printer

def getPrice(k, results):
    if k == 0:
        # print(threading.currentThread().getName() + '\n')
        results[k] = (laptop.join( \
                product, lambda s1: s1['model'], lambda s2: s2['model'], lambda result: result \
            ).where( \
                lambda x: x[1]['maker'] in \
                            pc.join(product, lambda s1: s1['model'], lambda s2: s2['model'], lambda result: result) \
                            .where(lambda x: x[0]['cd'] == pc.min(lambda y: y['cd'])) \
                            .select(lambda x: x[1]['maker']) \
                            .to_list() \
            ).order_by(lambda x: x[0]['price']) \
            .select(lambda x: x[0]['price']) \
            .take(1) \
            .to_list()[0]) #700
    elif k == 1:
        # print(threading.currentThread().getName() + '\n')
        results[k] = (pc.join( \
                product, lambda s1: s1['model'], lambda s2: s2['model'], lambda result: result \
            ).where( \
                lambda x: x[1]['maker'] in \
                            printer.join(product, lambda s1: s1['model'], lambda s2: s2['model'], lambda result: result) \
                            .where(lambda x: x[0]['price'] == printer.min(lambda y: y['price'])) \
                            .select(lambda x: x[1]['maker']) \
                            .to_list() \
            ).order_by_descending(lambda x: x[0]['price']) \
            .select(lambda x: x[0]['price']) \
            .take(1)
            .to_list()[0]) # 980
    elif k == 2:
        # print(threading.currentThread().getName() + '\n')
        results[k] = (printer.join( \
                product, lambda s1: s1['model'], lambda s2: s2['model'], lambda result: result \
            ).where( \
                lambda x: x[1]['maker'] in \
                            laptop.join(product, lambda s1: s1['model'], lambda s2: s2['model'], lambda result: result) \
                            .where(lambda x: x[0]['ram'] == laptop.max(lambda y: y['ram'])) \
                            .select(lambda x: x[1]['maker']) \
                            .to_list() \
            ).order_by_descending(lambda x: x[0]['price']) \
            .select(lambda x: x[0]['price']) \
            .take(1)
            .to_list()[0]) # 400
    elif k == 3:
        results[k] =  Enumerable([{'average': Enumerable([ \
            {'price': results[0] },
            {'price': results[1] },
            {'price': results[2] }
        ]).avg(lambda x: x['price']) }]).select(lambda x: round(x['average'], 2)).to_list()[0]

results = [0,0,0,0]
for i in range(4):
    t = threading.Thread(target=getPrice, args = (i,results))
    # print(t)
    t.start()


print(results[3])
end = time.time()
print("Execution time: " + str(end - start))
#
# print(
# Enumerable([{'average':
# Enumerable([

# ]).avg(lambda x: x['price']) }]).select(lambda x: round(x['average'], 2)).to_list()[0]
# )
