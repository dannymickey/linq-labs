from py_linq import Enumerable

import time
start = time.time()
from collections91 import utQ, utV, utB

answer = Enumerable([ \
    { 'division': (utB.sum(lambda x: x['B_VOL']) / utQ.count()) } \
]).select(lambda x: round(x['division'], 2)).to_list()[0]

end = time.time()
print(answer)
print("Execution time: " + str(end - start))
