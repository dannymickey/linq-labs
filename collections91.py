from py_linq import Enumerable

utQ = Enumerable([
{ 'Q_ID': 1,'Q_NAME': 'Square # 01'},
{ 'Q_ID': 10,'Q_NAME': 'Square # 10'},
{ 'Q_ID': 11,'Q_NAME': 'Square # 11'},
{ 'Q_ID': 12,'Q_NAME': 'Square # 12'},
{ 'Q_ID': 13,'Q_NAME': 'Square # 13'},
{ 'Q_ID': 14,'Q_NAME': 'Square # 14'},
{ 'Q_ID': 15,'Q_NAME': 'Square # 15'},
{ 'Q_ID': 16,'Q_NAME': 'Square # 16'},
{ 'Q_ID': 17,'Q_NAME': 'Square # 17'},
{ 'Q_ID': 18,'Q_NAME': 'Square # 18'},
{ 'Q_ID': 19,'Q_NAME': 'Square # 19'},
{ 'Q_ID': 2,'Q_NAME': 'Square # 02'},
{ 'Q_ID': 20,'Q_NAME': 'Square # 20'},
{ 'Q_ID': 21,'Q_NAME': 'Square # 21'},
{ 'Q_ID': 22,'Q_NAME': 'Square # 22'},
{ 'Q_ID': 23,'Q_NAME': 'Square # 23'},
{ 'Q_ID': 25,'Q_NAME': 'Square # 25'},
{ 'Q_ID': 3,'Q_NAME': 'Square # 03'},
{ 'Q_ID': 4,'Q_NAME': 'Square # 04'},
{ 'Q_ID': 5,'Q_NAME': 'Square # 05'},
{ 'Q_ID': 6,'Q_NAME': 'Square # 06'},
{ 'Q_ID': 7,'Q_NAME': 'Square # 07'},
{ 'Q_ID': 8,'Q_NAME': 'Square # 08'},
{ 'Q_ID': 9,'Q_NAME': 'Square # 09'}
])

utV = Enumerable([
{'V_ID': '1','V_NAME':  'Balloon # 01', 'V_COLOR':  'R'},
{'V_ID': '10','V_NAME':  'Balloon # 10', 'V_COLOR':  'R'},
{'V_ID': '11','V_NAME':  'Balloon # 11', 'V_COLOR':  'R'},
{'V_ID': '12','V_NAME':  'Balloon # 12', 'V_COLOR':  'R'},
{'V_ID': '13','V_NAME':  'Balloon # 13', 'V_COLOR':  'G'},
{'V_ID': '14','V_NAME':  'Balloon # 14', 'V_COLOR':  'G'},
{'V_ID': '15','V_NAME':  'Balloon # 15', 'V_COLOR':  'B'},
{'V_ID': '16','V_NAME':  'Balloon # 16', 'V_COLOR':  'B'},
{'V_ID': '17','V_NAME':  'Balloon # 17', 'V_COLOR':  'R'},
{'V_ID': '18','V_NAME':  'Balloon # 18', 'V_COLOR':  'G'},
{'V_ID': '19','V_NAME':  'Balloon # 19', 'V_COLOR':  'B'},
{'V_ID': '2','V_NAME':  'Balloon # 02', 'V_COLOR':  'R'},
{'V_ID': '20','V_NAME':  'Balloon # 20', 'V_COLOR':  'R'},
{'V_ID': '21','V_NAME':  'Balloon # 21', 'V_COLOR':  'G'},
{'V_ID': '22','V_NAME':  'Balloon # 22', 'V_COLOR':  'B'},
{'V_ID': '23','V_NAME':  'Balloon # 23', 'V_COLOR':  'R'},
{'V_ID': '24','V_NAME':  'Balloon # 24', 'V_COLOR':  'G'},
{'V_ID': '25','V_NAME':  'Balloon # 25', 'V_COLOR':  'B'},
{'V_ID': '26','V_NAME':  'Balloon # 26', 'V_COLOR':  'B'},
{'V_ID': '27','V_NAME':  'Balloon # 27', 'V_COLOR':  'R'},
{'V_ID': '28','V_NAME':  'Balloon # 28', 'V_COLOR':  'G'},
{'V_ID': '29','V_NAME':  'Balloon # 29', 'V_COLOR':  'R'},
{'V_ID': '3','V_NAME':  'Balloon # 03', 'V_COLOR':  'R'},
{'V_ID': '30','V_NAME':  'Balloon # 30', 'V_COLOR':  'G'},
{'V_ID': '31','V_NAME':  'Balloon # 31', 'V_COLOR':  'R'},
{'V_ID': '32','V_NAME':  'Balloon # 32', 'V_COLOR':  'G'},
{'V_ID': '33','V_NAME':  'Balloon # 33', 'V_COLOR':  'B'},
{'V_ID': '34','V_NAME':  'Balloon # 34', 'V_COLOR':  'R'},
{'V_ID': '35','V_NAME':  'Balloon # 35', 'V_COLOR':  'G'},
{'V_ID': '36','V_NAME':  'Balloon # 36', 'V_COLOR':  'B'},
{'V_ID': '37','V_NAME':  'Balloon # 37', 'V_COLOR':  'R'},
{'V_ID': '38','V_NAME':  'Balloon # 38', 'V_COLOR':  'G'},
{'V_ID': '39','V_NAME':  'Balloon # 39', 'V_COLOR':  'B'},
{'V_ID': '4','V_NAME':  'Balloon # 04', 'V_COLOR':  'G'},
{'V_ID': '40','V_NAME':  'Balloon # 40', 'V_COLOR':  'R'},
{'V_ID': '41','V_NAME':  'Balloon # 41', 'V_COLOR':  'R'},
{'V_ID': '42','V_NAME':  'Balloon # 42', 'V_COLOR':  'G'},
{'V_ID': '43','V_NAME':  'Balloon # 43', 'V_COLOR':  'B'},
{'V_ID': '44','V_NAME':  'Balloon # 44', 'V_COLOR':  'R'},
{'V_ID': '45','V_NAME':  'Balloon # 45', 'V_COLOR':  'G'},
{'V_ID': '46','V_NAME':  'Balloon # 46', 'V_COLOR':  'B'},
{'V_ID': '47','V_NAME':  'Balloon # 47', 'V_COLOR':  'B'},
{'V_ID': '48','V_NAME':  'Balloon # 48', 'V_COLOR':  'G'},
{'V_ID': '49','V_NAME':  'Balloon # 49', 'V_COLOR':  'R'},
{'V_ID': '5','V_NAME':  'Balloon # 05', 'V_COLOR':  'G'},
{'V_ID': '50','V_NAME':  'Balloon # 50', 'V_COLOR':  'G'},
{'V_ID': '51','V_NAME':  'Balloon # 51', 'V_COLOR':  'B'},
{'V_ID': '52','V_NAME':  'Balloon # 52', 'V_COLOR':  'R'},
{'V_ID': '53','V_NAME':  'Balloon # 53', 'V_COLOR':  'G'},
{'V_ID': '54','V_NAME':  'Balloon # 54', 'V_COLOR':  'B'},
{'V_ID': '6','V_NAME':  'Balloon # 06', 'V_COLOR':  'G'},
{'V_ID': '7','V_NAME':  'Balloon # 07', 'V_COLOR':  'B'},
{'V_ID': '8','V_NAME':  'Balloon # 08', 'V_COLOR':  'B'},
{'V_ID': '9','V_NAME':  'Balloon # 09', 'V_COLOR':  'B'},
])

utB = Enumerable([
{ 'B_DATETIME': '2000-01-01 01:13:36.000', 'B_Q_ID': 22, 'B_V_ID': 50, 'B_VOL':  50 },
{ 'B_DATETIME': '2001-01-01 01:13:37.000', 'B_Q_ID': 22, 'B_V_ID': 50, 'B_VOL':  50 },
{ 'B_DATETIME': '2002-01-01 01:13:38.000', 'B_Q_ID': 22, 'B_V_ID': 51, 'B_VOL':  50 },
{ 'B_DATETIME': '2002-06-01 01:13:39.000', 'B_Q_ID': 22, 'B_V_ID': 51, 'B_VOL':  50 },
{ 'B_DATETIME': '2003-01-01 01:12:01.000', 'B_Q_ID': 1, 'B_V_ID': 1, 'B_VOL':  155 },
{ 'B_DATETIME': '2003-01-01 01:12:03.000', 'B_Q_ID': 2, 'B_V_ID': 2, 'B_VOL':  255 },
{ 'B_DATETIME': '2003-01-01 01:12:04.000', 'B_Q_ID': 3, 'B_V_ID': 3, 'B_VOL':  255 },
{ 'B_DATETIME': '2003-01-01 01:12:05.000', 'B_Q_ID': 1, 'B_V_ID': 4, 'B_VOL':  255 },
{ 'B_DATETIME': '2003-01-01 01:12:06.000', 'B_Q_ID': 2, 'B_V_ID': 5, 'B_VOL':  255 },
{ 'B_DATETIME': '2003-01-01 01:12:07.000', 'B_Q_ID': 3, 'B_V_ID': 6, 'B_VOL':  255 },
{ 'B_DATETIME': '2003-01-01 01:12:08.000', 'B_Q_ID': 1, 'B_V_ID': 7, 'B_VOL':  255 },
{ 'B_DATETIME': '2003-01-01 01:12:09.000', 'B_Q_ID': 2, 'B_V_ID': 8, 'B_VOL':  255 },
{ 'B_DATETIME': '2003-01-01 01:12:10.000', 'B_Q_ID': 3, 'B_V_ID': 9, 'B_VOL':  255 },
{ 'B_DATETIME': '2003-01-01 01:12:11.000', 'B_Q_ID': 4, 'B_V_ID': 10, 'B_VOL':  50 },
{ 'B_DATETIME': '2003-01-01 01:12:12.000', 'B_Q_ID': 5, 'B_V_ID': 11, 'B_VOL':  100 },
{ 'B_DATETIME': '2003-01-01 01:12:13.000', 'B_Q_ID': 5, 'B_V_ID': 12, 'B_VOL':  155 },
{ 'B_DATETIME': '2003-01-01 01:12:14.000', 'B_Q_ID': 5, 'B_V_ID': 13, 'B_VOL':  155 },
{ 'B_DATETIME': '2003-01-01 01:12:15.000', 'B_Q_ID': 5, 'B_V_ID': 14, 'B_VOL':  100 },
{ 'B_DATETIME': '2003-01-01 01:12:16.000', 'B_Q_ID': 5, 'B_V_ID': 15, 'B_VOL':  50 },
{ 'B_DATETIME': '2003-01-01 01:12:17.000', 'B_Q_ID': 5, 'B_V_ID': 16, 'B_VOL':  205 },
{ 'B_DATETIME': '2003-01-01 01:12:18.000', 'B_Q_ID': 6, 'B_V_ID': 10, 'B_VOL':  155 },
{ 'B_DATETIME': '2003-01-01 01:12:19.000', 'B_Q_ID': 6, 'B_V_ID': 17, 'B_VOL':  100 },
{ 'B_DATETIME': '2003-01-01 01:12:20.000', 'B_Q_ID': 6, 'B_V_ID': 18, 'B_VOL':  255 },
{ 'B_DATETIME': '2003-01-01 01:12:21.000', 'B_Q_ID': 6, 'B_V_ID': 19, 'B_VOL':  255 },
{ 'B_DATETIME': '2003-01-01 01:12:22.000', 'B_Q_ID': 7, 'B_V_ID': 17, 'B_VOL':  155 },
{ 'B_DATETIME': '2003-01-01 01:12:23.000', 'B_Q_ID': 7, 'B_V_ID': 20, 'B_VOL':  100 },
{ 'B_DATETIME': '2003-01-01 01:12:24.000', 'B_Q_ID': 7, 'B_V_ID': 21, 'B_VOL':  255 },
{ 'B_DATETIME': '2003-01-01 01:12:25.000', 'B_Q_ID': 7, 'B_V_ID': 22, 'B_VOL':  255 },
{ 'B_DATETIME': '2003-01-01 01:12:26.000', 'B_Q_ID': 8, 'B_V_ID': 10, 'B_VOL':  50 },
{ 'B_DATETIME': '2003-01-01 01:12:27.000', 'B_Q_ID': 9, 'B_V_ID': 23, 'B_VOL':  255 },
{ 'B_DATETIME': '2003-01-01 01:12:28.000', 'B_Q_ID': 9, 'B_V_ID': 24, 'B_VOL':  255 },
{ 'B_DATETIME': '2003-01-01 01:12:29.000', 'B_Q_ID': 9, 'B_V_ID': 25, 'B_VOL':  100 },
{ 'B_DATETIME': '2003-01-01 01:12:30.000', 'B_Q_ID': 9, 'B_V_ID': 26, 'B_VOL':  155 },
{ 'B_DATETIME': '2003-01-01 01:12:31.000', 'B_Q_ID': 10, 'B_V_ID': 25, 'B_VOL':  155 },
{ 'B_DATETIME': '2003-01-01 01:12:31.000', 'B_Q_ID': 10, 'B_V_ID': 26, 'B_VOL':  100 },
{ 'B_DATETIME': '2003-01-01 01:12:33.000', 'B_Q_ID': 10, 'B_V_ID': 27, 'B_VOL':  10 },
{ 'B_DATETIME': '2003-01-01 01:12:34.000', 'B_Q_ID': 10, 'B_V_ID': 28, 'B_VOL':  10 },
{ 'B_DATETIME': '2003-01-01 01:12:35.000', 'B_Q_ID': 10, 'B_V_ID': 29, 'B_VOL':  245 },
{ 'B_DATETIME': '2003-01-01 01:12:36.000', 'B_Q_ID': 10, 'B_V_ID': 30, 'B_VOL':  245 },
{ 'B_DATETIME': '2003-01-01 01:12:37.000', 'B_Q_ID': 11, 'B_V_ID': 31, 'B_VOL':  100 },
{ 'B_DATETIME': '2003-01-01 01:12:38.000', 'B_Q_ID': 11, 'B_V_ID': 32, 'B_VOL':  100 },
{ 'B_DATETIME': '2003-01-01 01:12:39.000', 'B_Q_ID': 11, 'B_V_ID': 33, 'B_VOL':  100 },
{ 'B_DATETIME': '2003-01-01 01:12:40.000', 'B_Q_ID': 11, 'B_V_ID': 34, 'B_VOL':  155 },
{ 'B_DATETIME': '2003-01-01 01:12:41.000', 'B_Q_ID': 11, 'B_V_ID': 35, 'B_VOL':  155 },
{ 'B_DATETIME': '2003-01-01 01:12:42.000', 'B_Q_ID': 11, 'B_V_ID': 36, 'B_VOL':  155 },
{ 'B_DATETIME': '2003-01-01 01:12:43.000', 'B_Q_ID': 12, 'B_V_ID': 31, 'B_VOL':  155 },
{ 'B_DATETIME': '2003-01-01 01:12:44.000', 'B_Q_ID': 12, 'B_V_ID': 32, 'B_VOL':  155 },
{ 'B_DATETIME': '2003-01-01 01:12:45.000', 'B_Q_ID': 12, 'B_V_ID': 33, 'B_VOL':  155 },
{ 'B_DATETIME': '2003-01-01 01:12:46.000', 'B_Q_ID': 12, 'B_V_ID': 34, 'B_VOL':  100 },
{ 'B_DATETIME': '2003-01-01 01:12:47.000', 'B_Q_ID': 12, 'B_V_ID': 35, 'B_VOL':  100 },
{ 'B_DATETIME': '2003-01-01 01:12:48.000', 'B_Q_ID': 12, 'B_V_ID': 36, 'B_VOL':  100 },
{ 'B_DATETIME': '2003-01-01 01:13:01.000', 'B_Q_ID': 4, 'B_V_ID': 37, 'B_VOL':  20 },
{ 'B_DATETIME': '2003-01-01 01:13:02.000', 'B_Q_ID': 8, 'B_V_ID': 38, 'B_VOL':  20 },
{ 'B_DATETIME': '2003-01-01 01:13:03.000', 'B_Q_ID': 13, 'B_V_ID': 39, 'B_VOL':  123 },
{ 'B_DATETIME': '2003-01-01 01:13:04.000', 'B_Q_ID': 14, 'B_V_ID': 39, 'B_VOL':  111 },
{ 'B_DATETIME': '2003-01-01 01:13:05.000', 'B_Q_ID': 14, 'B_V_ID': 40, 'B_VOL':  50 },
{ 'B_DATETIME': '2003-01-01 01:13:05.000', 'B_Q_ID': 4, 'B_V_ID': 37, 'B_VOL':  185 },
{ 'B_DATETIME': '2003-01-01 01:13:06.000', 'B_Q_ID': 15, 'B_V_ID': 41, 'B_VOL':  50 },
{ 'B_DATETIME': '2003-01-01 01:13:07.000', 'B_Q_ID': 15, 'B_V_ID': 41, 'B_VOL':  50 },
{ 'B_DATETIME': '2003-01-01 01:13:08.000', 'B_Q_ID': 15, 'B_V_ID': 42, 'B_VOL':  50 },
{ 'B_DATETIME': '2003-01-01 01:13:09.000', 'B_Q_ID': 15, 'B_V_ID': 42, 'B_VOL':  50 },
{ 'B_DATETIME': '2003-01-01 01:13:10.000', 'B_Q_ID': 16, 'B_V_ID': 42, 'B_VOL':  50 },
{ 'B_DATETIME': '2003-01-01 01:13:11.000', 'B_Q_ID': 16, 'B_V_ID': 42, 'B_VOL':  50 },
{ 'B_DATETIME': '2003-01-01 01:13:12.000', 'B_Q_ID': 16, 'B_V_ID': 43, 'B_VOL':  50 },
{ 'B_DATETIME': '2003-01-01 01:13:13.000', 'B_Q_ID': 16, 'B_V_ID': 43, 'B_VOL':  50 },
{ 'B_DATETIME': '2003-01-01 01:13:14.000', 'B_Q_ID': 16, 'B_V_ID': 47, 'B_VOL':  50 },
{ 'B_DATETIME': '2003-01-01 01:13:15.000', 'B_Q_ID': 17, 'B_V_ID': 44, 'B_VOL':  10 },
{ 'B_DATETIME': '2003-01-01 01:13:16.000', 'B_Q_ID': 17, 'B_V_ID': 44, 'B_VOL':  10 },
{ 'B_DATETIME': '2003-01-01 01:13:17.000', 'B_Q_ID': 17, 'B_V_ID': 45, 'B_VOL':  10 },
{ 'B_DATETIME': '2003-01-01 01:13:18.000', 'B_Q_ID': 17, 'B_V_ID': 45, 'B_VOL':  10 },
{ 'B_DATETIME': '2003-01-01 01:13:24.000', 'B_Q_ID': 19, 'B_V_ID': 44, 'B_VOL':  10 },
{ 'B_DATETIME': '2003-01-01 01:13:25.000', 'B_Q_ID': 19, 'B_V_ID': 45, 'B_VOL':  10 },
{ 'B_DATETIME': '2003-01-01 01:13:26.000', 'B_Q_ID': 19, 'B_V_ID': 45, 'B_VOL':  10 },
{ 'B_DATETIME': '2003-02-01 01:13:19.000', 'B_Q_ID': 18, 'B_V_ID': 45, 'B_VOL':  10 },
{ 'B_DATETIME': '2003-02-01 01:13:27.000', 'B_Q_ID': 20, 'B_V_ID': 45, 'B_VOL':  10 },
{ 'B_DATETIME': '2003-02-01 01:13:31.000', 'B_Q_ID': 21, 'B_V_ID': 49, 'B_VOL':  50 },
{ 'B_DATETIME': '2003-02-02 01:13:32.000', 'B_Q_ID': 21, 'B_V_ID': 49, 'B_VOL':  50 },
{ 'B_DATETIME': '2003-02-03 01:13:33.000', 'B_Q_ID': 21, 'B_V_ID': 50, 'B_VOL':  50 },
{ 'B_DATETIME': '2003-02-04 01:13:34.000', 'B_Q_ID': 21, 'B_V_ID': 50, 'B_VOL':  50 },
{ 'B_DATETIME': '2003-02-05 01:13:35.000', 'B_Q_ID': 21, 'B_V_ID': 48, 'B_VOL':  1 },
{ 'B_DATETIME': '2003-03-01 01:13:20.000', 'B_Q_ID': 18, 'B_V_ID': 45, 'B_VOL':  10 },
{ 'B_DATETIME': '2003-03-01 01:13:28.000', 'B_Q_ID': 20, 'B_V_ID': 45, 'B_VOL':  10 },
{ 'B_DATETIME': '2003-04-01 01:13:21.000', 'B_Q_ID': 18, 'B_V_ID': 46, 'B_VOL':  10 },
{ 'B_DATETIME': '2003-04-01 01:13:29.000', 'B_Q_ID': 20, 'B_V_ID': 46, 'B_VOL':  10 },
{ 'B_DATETIME': '2003-05-01 01:13:22.000', 'B_Q_ID': 18, 'B_V_ID': 46, 'B_VOL':  10 },
{ 'B_DATETIME': '2003-05-01 01:13:30.000', 'B_Q_ID': 20, 'B_V_ID': 46, 'B_VOL':  10 },
{ 'B_DATETIME': '2003-06-11 01:13:23.000', 'B_Q_ID': 19, 'B_V_ID': 44, 'B_VOL':  10 },
{ 'B_DATETIME': '2003-06-23 01:12:02.000', 'B_Q_ID': 1, 'B_V_ID': 1, 'B_VOL':  100 },
])
